FROM node:16-bullseye

LABEL   maintainer="erin" \
        source="https://gitlab.com/aria.theater/aerithe" \
        issues="https://gitlab.com/aria.theater/aerithe/-/issues"

# Install system dependencies
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    supervisor \
    net-tools \
    dnsutils \
    vim \
    telnet \
    less \
    postgresql-client
RUN apt-get clean && \
  rm -rf /var/lib/apt/lists/*

# Copy app source code
WORKDIR /usr/src/aerithe/
ENV APP_CONFIG=/var/lib/aerithe \
    NODE_ENV=development
ADD src ./

# Install nodejs dependencies
RUN npm install

# Configure supervisord
COPY ./supervisor.conf /etc/supervisor/conf.d/aerithe.conf
COPY ./supervisord-watchdog /etc/supervisor/
RUN chmod +x /etc/supervisor/supervisord-watchdog

# Configure container runtime
EXPOSE 80
VOLUME /var/lib/aerithe
CMD ["/usr/bin/supervisord"]
