CREATE TABLE IF NOT EXISTS users (
    id              bigint          PRIMARY KEY,
    tag             text            NOT NULL,
    authorized      boolean         NOT NULL,
    joined          timestamp       NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS idx_users_authorized
    ON users (id, authorized);

CREATE TABLE IF NOT EXISTS user_tokens (
    "user_id"       bigint          REFERENCES users(id) ON DELETE CASCADE,
    bearer_token    text            NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS idx_tokens_user
    ON user_tokens ("user_id");