const { ENV } = require('./config')

const pg = require('./services/postgresql')
const bot = require('./services/bot')

const express = require('express')
const { router } = require('./router.js')
const cookieParser = require('cookie-parser')
const http = require('http')

const util = require('util')
const path = require('path')

async function init() {
    pg.init()

    const app = express()
    const server = http.createServer(app)

    app.use(async (req, res, next) => {
        if (req.hostname !== ENV.APP_VHOST)
            return res.redirect(`http://${ENV.APP_VHOST}:${ENV.APP_PORT}${req.originalUrl}`);

        next()
    })

    app.use(express.json())
    app.use(cookieParser());

    app.use('', router)

    bot.init()

    app.use(function onError(err, req, res, next) {
        res.statusCode = 500;

        if (ENV.NODE_ENV == 'development') {
            // return error & request details as JSON
            res.end(`ERROR: ${util.inspect(err)}`);
         } else {
            // return generic error page
            var options = { root: path.join(__dirname) }
            res.sendFile('public/500.html', options);
        }
    });

    server.listen(80)
}

init();
