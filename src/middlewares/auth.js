const atob = require('atob')

if (ENV.DISCORD_ADMIN_IDS)
    ENV.DISCORD_ADMIN_IDS = ENV.DISCORD_ADMIN_IDS.split(',');

function checkAuth(req, res, next) {
    console.log(req.cookies)
    if (!req.cookies.discord)
        return res.status(403).redirect('/login');

    let cookie = JSON.parse(atob(req.cookies.discord))
    // console.log(cookie)
    if (!users.includes(JSON.stringify(cookie)))
        return res.status(403).redirect('/login');

    next()
}

function checkAdminAuth(req, res, next) {
    let user;

    if (req.cookies.discord && users.includes(atob(req.cookies.discord)))
        user = JSON.parse(atob(req.cookies.discord));

    if (user.id != ENV.DISCORD_OWNER_ID || !DISCORD_ADMIN_IDS.includes(user.id))
        return res.status(403).redirect('/');

    next()
}

function checkOwnerAuth(req, res, next) {
    let user;
    if (req.cookies.discord && users.includes(atob(req.cookies.discord)))
        user = JSON.parse(atob(req.cookies.discord))

    if (user.id != ENV.DISCORD_OWNER_ID)
        return res.status(403).redirect('/');

    next()
}