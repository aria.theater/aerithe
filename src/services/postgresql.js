const pg = require('pg')

let pool

async function init() {
    pool = new pg.Pool()
}

async function getConnection() {
    try {
        const client = await pool.connect()
        return client;
    } catch(err) {
        console.error(err)
        process.exit(1)
    }
}

module.exports = { init, getConnection };