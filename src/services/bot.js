const {
    ENV,
    DISCORD_CMD_PREFIX
} = require('../config')

//const pg = require('../services/postgresql')
const Discord = require('discord.js')

console.log('bot start');

async function init() {
    const client = new Discord.Client({
        intents: [
            Discord.Intents.FLAGS.GUILDS,
            Discord.Intents.FLAGS.GUILD_MEMBERS,
            Discord.Intents.FLAGS.GUILD_WEBHOOKS,
            Discord.Intents.FLAGS.GUILD_MESSAGES,
            Discord.Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
            Discord.Intents.FLAGS.GUILD_MESSAGE_TYPING
        ]
    });

    client.login(ENV.DISCORD_BOT_TOKEN)

    client.on('ready', () => {
        console.log(`Logged in as ${client.user.tag}`);
        console.log(`Online, serving ${client.users.cache.size} users in ${client.channels.cache.size} channels and threads across ${client.guilds.cache.size} guild(s)`);    
        client.user.setPresence({status: 'dnd'});
    });

    client.on("warn", data => {
        console.log(`Caught a warning: ${info}`);
    });
    client.on("error", error => {
        console.error(`WebSocket encountered a connection error: ${error}`);
    });

    client.on("guildUnavailable", data => {
        console.error(`Guild ${data} has become unavailable (network issue, or Dsicord outage?)`);
    });

    client.on("reconnecting", data => {
        console.error(`WebSocket connection lost! Attempting to reconnect`);
    });
    client.on("resume", data => {
        console.log(`WebSocker connection resumed! Replaying ${data} events`);
    });
    client.on("disconnect", event => {
        console.error(`WebSocket stream has been permanently lost`);
    });

    client.on('message', async (message) => {
        console.log(message.content);
        if (message.author.bot) return;
        if (!message.guild) return;
        const prefixRegex = new RegExp(
            `^(<@!?${client.user.id}>|${escapeRegex(DISCORD_CMD_PREFIX)})\s*`
        );
        if (!prefixRegex.test(message.content)) return;
        const [, matchedPrefix] = message.content.match(prefixRegex);
        const args = message.content.slice(matchedPrefix.length).trim().split(/ +/);
        const command = args.shift().toLowerCase();

        if (command == 'ping') {
            String.prototype.toHHMMSS = function () {
                var sec_num = parseInt(this, 10); // don't forget the second param
                var hours = Math.floor(sec_num / 3600);
                var minutes = Math.floor((sec_num - hours * 3600) / 60);
                var seconds = sec_num - hours * 3600 - minutes * 60;

                if (hours < 10) {
                    hours = '0' + hours;
                }
                if (minutes < 10) {
                    minutes = '0' + minutes;
                }
                if (seconds < 10) {
                    seconds = '0' + seconds;
                }
                var time = hours + ':' + minutes + ':' + seconds;

                return time;
            };
            var time = process.uptime();
            var uptime = (time + '').toHHMMSS();

            function format(mem) {
                return `${Math.round((mem / 1024 / 1024) * 100) / 100} MB`;
            }

            var process = require('process')
            var os = require('os')
            var embed = new Discord.MessageEmbed()
                .setTitle('Server Statistics')
                .setColor('#2BF49F')
                .setFooter(
                'made by Nate#1234',
                'https://cdn.discordapp.com/avatars/328680177066442752/a_f0e2483596ce8de77484ab7ae13abb0e?size=128'
                )
                .addFields(
                    {
                        name: ':robot: CPU Utilization',
                        value: `User: ${process.cpuUsage().user}\nSystem: ${process.cpuUsage().system
                        }`,
                    },
                    {
                        name: ':kissing: RAM Usage',
                        value: `Free: ${format(os.freemem())}\nCapacity: ${format(os.totalmem())}`,
                    },
                    {
                        name: ':rolling_eyes: Uptime',
                        value: uptime,
                    },
                    {
                        name: ':ping_pong: Latency',
                        value: `${msg.createdTimestamp - message.createdTimestamp}ms`,
                    },
                    {
                        name: ':scream: API Latency',
                        value: `${Math.round(client.ws.ping)}ms`,
                    }
                )
                .setTimestamp();

            message.channel.send(embed);
        }

/*
        if (command == 'userinfo') {
            var targetuser = message.mentions.users.first()
                ? message.mentions.users.first().id
                : args[0]
                    ? args[0]
                    : message.author.id;
            var result = await con.secure_query(
                "SELECT * FROM users WHERE discord_id = '$key0' OR i = '$key0';",
                targetuser
            );
            console.log(result);

            var userdata = dc[String(result.result[0].discord_id)];
            try {
                userdata.username = atob(userdata.username)
                userdata.tag = atob(userdata.tag)
            } catch {}
            console.log(
                result.result[0].discord_id,
                dc[String(result.result[0].discord_id)],
                userdata
            );

            if (!userdata || !result.result[0])
            return message.channel.send('This user does not appear to have an account!');

            var embed = new MessageEmbed()
                .setTitle(`Userinfo for ${userdata.tag}`)
                .setColor('3392FF')
                .addField('Username', userdata.tag)
                .addField('ID', result.result[0].i);
            message.channel.send(embed);
        }

        if (command == 'lookup') {
            var id = args[0];

            axios.request({
                url: 'https://discord.com/api/users/' + id,
                method: 'get',
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    Authorization: 'Bot ' + client.token,
                }
            })
            .then(async (m) => {
                var pfpuri = `https://cdn.discordapp.com/avatars/${m.data.id}/${m.data.avatar}?size=128`;
                var id = m.data.id;
                var tag = `${m.data.username}#${m.data.discriminator}`;
                var birth = m.data.createdAt.toString();

                var flags = m.data.public_flags;
                const Discord_Employee = 1;
                const Partnered_Server_Owner = 2;
                const HypeSquad_Events = 4;
                const Bug_Hunter_Level_1 = 8;
                const House_Bravery = 64;
                const House_Brilliance = 128;
                const House_Balance = 256;
                const Early_Supporter = 512;
                const Bug_Hunter_Level_2 = 16384;
                const Early_Verified_Bot_Developer = 131072;

                if ((flags & Discord_Employee) == Discord_Employee) {
                    var badge_Discord_Employee = 'true';
                } else {
                    var badge_Discord_Employee = 'false';
                }

                if ((flags & Partnered_Server_Owner) == Partnered_Server_Owner) {
                    var badge_Partnered_Server_Owner = 'true';
                } else {
                    var badge_Partnered_Server_Owner = 'false';
                }

                if ((flags & HypeSquad_Events) == HypeSquad_Events) {
                    var badge_HypeSquad_Events = 'true';
                } else {
                    var badge_HypeSquad_Events = 'false';
                }

                if ((flags & Bug_Hunter_Level_1) == Bug_Hunter_Level_1) {
                    var badge_Bug_Hunter_Level_1 = 'true';
                    var badge_bughunter = 'true';
                } else {
                    var badge_Bug_Hunter_Level_1 = 'false';
                }

                if ((flags & Bug_Hunter_Level_2) == Bug_Hunter_Level_2) {
                    var badge_Bug_Hunter_Level_2 = 'true';
                    var badge_bughunter = 'true';
                } else {
                    var badge_Bug_Hunter_Level_2 = 'false';
                }

                if ((flags & House_Bravery) == House_Bravery) {
                    var badge_House_Bravery = 'true';
                } else {
                    var badge_House_Bravery = 'false';
                }

                if ((flags & House_Brilliance) == House_Brilliance) {
                    var badge_House_Brilliance = 'true';
                } else {
                    var badge_House_Brilliance = 'false';
                }

                if ((flags & House_Balance) == House_Balance) {
                    var badge_House_Balance = 'true';
                } else {
                    var badge_House_Balance = 'false';
                }

                if ((flags & Early_Supporter) == Early_Supporter) {
                    var badge_Early_Supporter = 'true';
                } else {
                    var badge_Early_Supporter = 'false';
                }

                if ((flags & Early_Verified_Bot_Developer) == Early_Verified_Bot_Developer) {
                    var badge_Early_Verified_Bot_Developer = 'true';
                } else {
                    var badge_Early_Verified_Bot_Developer = 'false';
                }

                const embed = new Discord
                    .MessageEmbed()
                    .setTitle('Server Statistics')
                    .setColor('#2BF49F')
                    .setThumbnail(pfpuri)
                    .addFields(
                        {
                            name: 'Handle',
                            value: tag,
                        },
                        {
                            name: 'Account Birth',
                            value: birth
                        },
                        {
                            name: 'Flags',
                            value: flags,
                        },
                        {
                            name: 'Discord Staff',
                            value: badge_Discord_Employee
                        },
                        {
                            name: 'Bug Reporter',
                            value: badge_bughunter
                        }
                    )
                    .setTimestamp();

                message.channel.send(embed);

            })
            .catch((e) => {
                console.log(e);
            });
        };
*/
    });
}


module.exports = { init };