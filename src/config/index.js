require('dotenv').config()
const fs = require('fs/promises')

module.exports = {
    ENV: { ...process.env },
    DISCORD_CMD_PREFIX: '@'
}