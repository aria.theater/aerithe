const { authController, loginController } = require('./controllers/auth')
const express = require('express')

const router = express.Router()

router.get('/auth', authController);
router.get('/login', loginController);

module.exports = { router }