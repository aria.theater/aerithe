const { ENV } = require('../config')

const pg = require('../services/postgresql')

const axios = require('axios')
const { URLSearchParams } = require('url')

const fs = require('fs/promises')
const btoa = require('btoa')

const discordEndpoint = 'https://discordapp.com/api/v9'

async function requestToken(code, creds) {
    const data = new URLSearchParams();
    data.append('grant_type', 'authorization_code')
    data.append('client_id', ENV.DISCORD_CLIENT_ID)
    data.append('client_secret', ENV.DISCORD_CLIENT_SECRET)
    data.append('redirect_uri', `http://${ENV.APP_VHOST}:${ENV.APP_PORT}/auth`)
    data.append('code', code)

    const response = await axios.request({
        method: 'post',
        url: `${discordEndpoint}/oauth2/token`,
        data: data,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            // 'Authorization': `Basic ${creds}`
        }
    });

    return response.data;
}

async function discordApiRequest(
    { method, path, token, params=false } = {}
) {
    let options = {
        method: method,
        url: `${discordEndpoint}/${path}`,
        headers: { Authorization: `Bearer ${token}` }
    }

    if (options) {
        if (method == 'get')
            options.params = params;
        else if (method == 'post')
            options.data = params;
    }

    const response = await axios.request(options);
    return response.data;
}

async function authController(req, res) {
    if (!req.query.code) throw new Error('NoCodeProvided');

    try {
        // basic authentication
        const creds = btoa(`${ENV.DISCORD_CLIENT_ID}:${ENV.DISCORD_CLIENT_SECRET}`);

        // complete oauth2 code flow
        const tokenApiResponse = await requestToken(req.query.code, creds)
        if (!tokenApiResponse)
            throw new Error('DiscordServiceError');
        // console.log(tokenApiResponse)
        const accessToken = tokenApiResponse.access_token

        // verify user's token
        const userData = await discordApiRequest({
            method: 'get',
            path: 'users/@me',
            token: accessToken
        });
        if (!userData)
            throw new Error('DiscordServiceError');
        // console.log(userData)

        // is the user on our guild?
        const userGuilds = await discordApiRequest({
            method: 'get',
            path: 'users/@me/guilds',
            token: accessToken
        });
        if (!userGuilds)
            throw new Error('DiscordServiceError');
        // console.log(userGuilds)

        userData.authorized = false;
        userData.joined_at = null;
        if (userGuilds.find(guild => guild.id === ENV.DISCORD_GUILD_ID)) {
            userData.authorized = true;

            // when did the user join, if so?
            const guildMember = await discordApiRequest({
                method: 'get',
                path: `users/@me/guilds/${ENV.DISCORD_GUILD_ID}/member`,
                token: accessToken
            });
            if (!guildMember)
                throw new Error('DiscordServiceError');
            // console.log(guildMember)

            userData.joined_at = guildMember.joined_at;
        }

        let con = await pg.getConnection()
        const userExists = await con.query(
            `SELECT COUNT(*) AS "bool" FROM users WHERE id = $1`,
            [userData.id]
        )
        // console.log(`user existing? ${userExists.rows[0].bool}`)
        if (userExists.rows[0].bool == 0) {
            // create the user in database
            const userSql = `
                INSERT INTO users ("id", "tag", "authorized", "joined")
                    VALUES ($1, $2, $3, $4);
            `;
            const userValues = [
                userData.id,
                `${userData.username}#${userData.discriminator}`,
                userData.authorized,
                userData.joined_at
            ]
            await con.query(userSql, userValues)

            // link the bearer token to the user
            const tokenSql = `
                INSERT INTO user_tokens ("user_id", "bearer_token")
                    VALUES ($1, $2);
            `;
            const tokenValues = [userData.id, accessToken]
            await con.query(tokenSql, tokenValues)
        }
        con.release()

        res.cookie('discord', btoa(userData.id))
        res.status(200).send("authorized!") // JSON.stringify(userData)
    } catch(e) {
        console.error(e);
        res.status(500).send(e);
    }

}

async function loginController(req, res) {
    res.redirect(`https://discord.com/api/oauth2/authorize?client_id=${ENV.DISCORD_CLIENT_ID}&amp;redirect_uri=http://${ENV.APP_VHOST}:${ENV.APP_PORT}/auth&amp;response_type=code&amp;scope=identify%20guilds%20guilds.members.read`)
}

module.exports = { authController, loginController };
